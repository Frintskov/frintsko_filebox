import React from "react";
import navbarStyles from "./Navbar.module.css";
import { NavLink } from "react-router-dom";

const Navbar = () => {
  return (
    <div className={navbarStyles.wrapper}>
      <div className={navbarStyles.navWrapper}>
        <ul className={navbarStyles.navList}>
          <NavLink
            to="/homepage"
            className={navbarStyles.navLink}
            activeClassName={navbarStyles.active}
          >
            <li>Home page</li>
          </NavLink>
          <NavLink
            to="/allfiles"
            className={navbarStyles.navLink}
            activeClassName={navbarStyles.active}
          >
            <li>All files</li>
          </NavLink>
          <NavLink
            to="/last"
            className={navbarStyles.navLink}
            activeClassName={navbarStyles.active}
          >
            <li>Last</li>
          </NavLink>
          <NavLink
            to="/checked"
            className={navbarStyles.navLink}
            activeClassName={navbarStyles.active}
          >
            <li>Checked</li>
          </NavLink>
          <NavLink
            to="/general"
            className={navbarStyles.navLink}
            activeClassName={navbarStyles.active}
          >
            <li>General folder</li>
          </NavLink>
          <NavLink
            to="/request"
            className={navbarStyles.navLink}
            activeClassName={navbarStyles.active}
          >
            <li>Request files</li>
          </NavLink>
          <NavLink
            to="/deleted"
            className={navbarStyles.navLink}
            activeClassName={navbarStyles.active}
          >
            <li>Deleted files</li>
          </NavLink>
        </ul>
      </div>
    </div>
  );
};

export default Navbar;
