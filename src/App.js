import React, { useEffect } from "react";

import Header from "./Header/Header";
import Navbar from "./Navbar/Navbar";
import Content from "./Content/Content";
import AllFiles from "./Content/AllFiles/AllFiles";
import Last from "./Content/Last/Last";
import Checked from "./Content/Checked/Checked";
import General from "./Content/General/General";
import Request from "./Content/Request/Request";
import Deleted from "./Content/Deleted/Deleted";
import Tools from "./Tools/Tools";
import state from "./state/state";
import "./App.css";

import { Dropbox } from "dropbox";
import fetch from "isomorphic-fetch";
import { BrowserRouter } from "react-router-dom";
import { Route } from "react-router-dom";

function App() {
  useEffect(() => {
    const dbx = new Dropbox({
      accessToken:
        "hmwt_1m9xq4AAAAAAAAAAdB_-Eizgzi5vVX09RnT4thBF7S6Y0TYayI7bJyr1COv",
      fetch,
    });

    dbx
      .filesListFolder({
        path: "",
      })
      .then((res) => (state.files = [...state.files, ...res.result.entries]));
  }, []);
  if (state.files === []) {
    return null;
  } else {
    return (
      <div className="wrapper">
        <BrowserRouter>
          <div className="header">
            <Header state={state} />
          </div>
          <div className="navbar">
            <Navbar />
          </div>
          <div className="content">
            <Route
              path="/homepage"
              render={() => <Content state={state} />}
            ></Route>
            <Route path="/allfiles" render={() => <AllFiles />}></Route>
            <Route path="/last" render={() => <Last />}></Route>
            <Route path="/checked" render={() => <Checked />}></Route>
            <Route path="/general" render={() => <General />}></Route>
            <Route path="/request" render={() => <Request />}></Route>
            <Route path="/deleted" render={() => <Deleted />}></Route>
          </div>
          <aside>
            <Tools state={state} />
          </aside>
        </BrowserRouter>
      </div>
    );
  }
}

export default App;
