import React from "react";
import headerStyle from "./Header.module.css";
import Modal from "../Modal/Modal";
import "../bootstrap/bootstrap.min.css";
import { useState } from "react";
import { NavLink } from "react-router-dom";

const Header = (props) => {
  const [modalActive, setModalActive] = useState(false);
  const [width, setWidht] = useState("300px");

  const toggleWidth = () => {
    return width == "300px" ? setWidht("500px") : setWidht("300px");
  };

  return (
    <div className={headerStyle.headerWrapper}>
      <header>
        <div className={headerStyle.logoWrapper}>
          <div className={headerStyle.menu}>
            <nav className="navbar navbar-light ">
              <div className="container-fluid">
                <button
                  onClick={() => setModalActive(true)}
                  className="navbar-toggler"
                  type="button"
                  data-bs-toggle="collapse"
                  data-bs-target="#navbarToggleExternalContent"
                  aria-controls="navbarToggleExternalContent"
                  aria-expanded="false"
                  aria-label="Toggle navigation"
                >
                  <span className="navbar-toggler-icon"></span>
                </button>
              </div>
            </nav>
          </div>
          <NavLink to="/homepage" className={headerStyle.navLink}>
            <div className={headerStyle.logo}>
              <img
                className={headerStyle.logoImg}
                src={props.state.header.logo}
                alt="logo"
              />
            </div>
            <div className={headerStyle.sign}>BOX</div>
          </NavLink>
        </div>
        <div className={headerStyle.userField}>
          <div className={headerStyle.search}>
            <form className="d-flex">
              <input
                className="form-control me-2"
                type="search"
                placeholder="Search"
                aria-label="Search"
                onFocus={toggleWidth}
                onBlur={toggleWidth}
                style={{ width: width }}
              />
            </form>
          </div>
          <div
            className={headerStyle.user}
            onClick={() => setModalActive(true)}
          >
            <div className={headerStyle.userIcon}>
              <img src={props.state.header.myPhoto} alt="my photo" />
            </div>
            <div className={headerStyle.userName}>
              {props.state.userInfo.name}
            </div>
          </div>
          <div className={headerStyle.search}></div>
        </div>
      </header>
      <Modal active={modalActive} setActive={setModalActive} />
    </div>
  );
};

export default Header;
