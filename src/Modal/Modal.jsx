import React from "react";
import modStyles from "./Modal.module.css";
import "../bootstrap/bootstrap.min.css";

const Modal = ({ active, setActive }) => {
  return (
    <div
      className={
        active ? `${modStyles.modal} ${modStyles.active}` : modStyles.modal
      }
      onClick={() => setActive(false)}
    >
      <div
        className={modStyles.modalContent}
        onClick={(e) => e.stopPropagation()}
      >
        <div>Not implemented</div>
      </div>
    </div>
  );
};

export default Modal;
