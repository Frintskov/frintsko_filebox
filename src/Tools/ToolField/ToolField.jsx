import React, { useState } from "react";
import toolStyle from "./Toolstyle.module.css";
import Modal from "../../Modal/Modal";

const ToolField = (props) => {
  const [modalActive, setModalActive] = useState(false);

  return (
    <div>
      <div
        className={toolStyle.folderItem}
        onClick={() => {
          setModalActive(true);
        }}
      >
        <div className={toolStyle.toolImg}>
          <svg
            viewBox="0 0 24 24"
            fill="none"
            className="dig-UIIcon dig-UIIcon--standard"
            width="24"
            height="24"
            focusable="false"
          >
            <path d={props.d1} fill="currentColor"></path>
            <path d={props.d2} fill="currentColor"></path>
          </svg>
        </div>
        <div className={toolStyle.toolText}>{props.text}</div>
      </div>
      <Modal active={modalActive} setActive={setModalActive} />
    </div>
  );
};

export default ToolField;
