import React, { useState } from "react";
import "../bootstrap/bootstrap.min.css";
import toolStyle from "./Tools.module.css";
import Modal from "../Modal/Modal";
import ToolField from "./ToolField/ToolField";

const Tools = (props) => {
  const [modalActive, setModalActive] = useState(false);

  return (
    <div className={toolStyle.wrapper}>
      <div className="dropdown">
        <button
          onClick={() => setModalActive(true)}
          className="btn btn-secondary dropdown-toggle"
          type="button"
          id="dropdownMenuButton1"
          data-bs-toggle="dropdown"
          aria-expanded="false"
        >
          Create
        </button>
      </div>
      <div className={toolStyle.foldersTools}>
        {props.state.toolFields.map((field) => {
          return <ToolField text={field.text} d1={field.d1} d2={field.d2} />;
        })}
      </div>
      <Modal active={modalActive} setActive={setModalActive} />
    </div>
  );
};

export default Tools;
