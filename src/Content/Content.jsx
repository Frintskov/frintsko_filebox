import React from "react";
import contentStyle from "./Content.module.css";
import RenderFiles from "./RenderFiles/RenderFiles";

const Content = (props) => {
  return (
    <div>
      <div className={contentStyle.filesDescription}>
        <div>Check</div>
        <div>Name</div>
        <div>Data</div>
        <div></div>
      </div>
      {props.state.files
        .sort((a, b) => {
          if (
            (a[".tag"] === "folder" || b[".tag"] === "folder") &&
            !(a[".tag"] === b[".tag"])
          ) {
            return a[".tag"] === "folder" ? -1 : 1;
          } else {
            return a.name.toLowerCase() < b.name.toLowerCase() ? -1 : 1;
          }
        })
        .map((file) => {
          const type = file[".tag"];
          return (
            <div>
              <RenderFiles
                type={type}
                name={file.name}
                data={file.client_modified}
              />
            </div>
          );
        })}
    </div>
  );
};

export default Content;
