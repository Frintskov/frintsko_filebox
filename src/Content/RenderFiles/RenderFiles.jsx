import React, { useState } from "react";
import s from "./RenderFiles.module.css";
import Modal from "../../Modal/Modal";

const RenderFiles = (props) => {
  const [state, setState] = useState(null);
  const [modalActive, setModalActive] = useState(false);

  const changeWeight = () => {
    return props.type === "folder" ? "700" : "400";
  };

  const dataNormalize = (data) => {
    if (data) {
      data = data.split("T");

      return data[0];
    }
  };
  const showRemove = () => {
    setState("remove");
  };

  const hideRemove = () => {
    setState("");
  };

  return (
    <div
      className={s.fileWrapper}
      onMouseOver={showRemove}
      onMouseLeave={hideRemove}
    >
      <div>
        <input
          className={`form-check-input ${s.checkbox}`}
          type="checkbox"
          id="checkboxNoLabel"
          value=""
          aria-label="..."
        />
      </div>
      <div className={s.name} style={{ fontWeight: changeWeight() }}>
        {props.name}
      </div>
      <div className={s.data}>{dataNormalize(props.data)}</div>
      <div className={s.remove}>
        <span
          className={s.removeText}
          onClick={() => {
            setModalActive(true);
          }}
        >
          {state}
        </span>
      </div>
      <Modal active={modalActive} setActive={setModalActive} />
    </div>
  );
};

export default RenderFiles;
